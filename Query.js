

class Query {
	constructor(name, options) {
		this.name = `${name ? `${name}_` : ``}${this.constructor.name}`;
		this.errors = [];
		this.failed = false;
		this.success = false;
		this.options = Object.assign({
			// options?
			startDate: undefined, // Date
			endDate: undefined, // Date
			filters: undefined // [{field: String, excludeIfContains: [String], includeIfContains: [String]}] 
		}, options || {});
		this.numFiltered = 0;
		this.records = [];
	}

	_errorOccurred(error, message, log = true) {
		this.errors.push({error, message});
		if (log) {
			console.log(`${this.name} | ${message} | ${error}`)
		}
	}

	async execute(connection) {
		let result = await new Promise((resolve, reject) => {
			let soql = this._generateSoql();

			console.log(`${this.name} | executing...`);

			let container = {
				task: undefined
			}

			container.task = connection
				.query(soql)
				.on("error", error => {
					try {
						this._errorOccurred(error, `query error`);
					} catch (err) {
						this._errorOccurred(err, `error in query .on("error")`);
					}
					this.failed = true;
					this._onComplete();
					reject(error);
				});

			this._applyOnRecord(container);
		
			container.task = container.task.on(`end`, async query => {
				this._onEnd(query);
				let result = {records: this.records};
				this._applyModifiers(result);
				resolve(result.records);
			});

			container.task = container.task.execute({autoFetch: true});
		});

		if (this.errors && this.errors.length > 0) {
			throw this.errors;
		}

		return result;
	}

	_generateSoql() {
		return "";
	}
	
	_generateDateClause() {
		let {startDate, endDate} = this.options;
		if (startDate && endDate) {
			return `(CreatedDate >= ${startDate.toISOString()} AND CreatedDate <= ${endDate.toISOString()})`;
		} else if (startDate) {
			return `(CreatedDate >= ${startDate.toISOString()})`;
		} else if (endDate) {
			return `(CreatedDate <= ${endDate.toISOString()})`;
		} else {
			return `(CreatedDate = THIS_YEAR)`;
		}
	}

	_applyOnRecord(container) {
		container.task = container.task.on(`record`, record => {
			try {
				this._onRecord(record);
			} catch (error) {
				this._errorOccurred(error, `error in .on("record")`);
			}
		});
	}
	_onRecord(record) {
		let relationshipFields = this._getRelationshipFields(record, null);
		relationshipFields.forEach(relationshipKey => {
			//console.log(`Relationship key: ${relationshipKey}`);
			let pathElements = relationshipKey.split(".");
			let value = pathElements.reduce((res,pathElement) => res[pathElement], record);
			record[relationshipKey] = value;
		});
		this.records.push(record);
		// modify or use info from record.
	}
	_getRelationshipFields(object, parentKey) {
		let relationshipKeys = Object.keys(object).filter(key => {
			return object[key] && (typeof object[key]) == `object` && key != `attributes`;
		});
		/*if (relationshipKeys.length > 0) {
			console.log("BOOM");
			console.log(relationshipKeys.map(key => { return {key, type: typeof object[key], value: object[key]}; }));
		}*/
		let relationshipFields = [];
		if (parentKey) {
			let leafKeys = Object.keys(object).filter(key => {
				return (typeof object[key]) != `object` && key != `attributes`;
			});
			leafKeys.forEach(leafKey => {
				//console.log(`Leaf key: ${leafKey}`);
				relationshipFields.push(parentKey + `.` + leafKey);
			});
		}
		relationshipKeys.forEach(key => {
			if (parentKey) {
				relationshipFields = relationshipFields.concat(this._getRelationshipFields(object[key], parentKey + "." + key));
			} else {
				relationshipFields = relationshipFields.concat(this._getRelationshipFields(object[key], key));
			}
		});
		return relationshipFields;
	}



	_applyModifiers(result) {
		this._applyFilter(result);
		this._applyMap(result);
		this._reduce(result);
	}

	_applyFilter(result) {
		try {
			result.records = result.records.filter(record => {
				try {
					let passesFilter = this._filter(record);
					if (!passesFilter) {
						this.numFiltered += 1;
					}
					return passesFilter;
				} catch (error) {
					this._errorOccurred(error, `Error in _filter`);
					return true;
				}
			});
		} catch (error) {
			this._errorOccurred(error, `Error during _applyFilter`);
		}
	}

	_filter(record) {
		let filters = this.options.filters;
		if (!filters) {
			return true;
		}
		for (let i = 0; i < filters.length; i++) {
			let { field, excludeIfContains, includeIfContains } = filters[i];
			if (excludeIfContains && fieldContainsOneOfValues(record, field, excludeIfContains)) {
				return false;
			}
			if (includeIfContains && includeIfContains.length > 0 && fieldDoesNotContainValues(record, field, includeIfContains)) {
				return false;
			}
		}
		return true;
	}

	_applyMap(result) {
		try {
			result.records = result.records.map(record => { 
				try {
					return this._map(record);
				} catch (error) {
					this._errorOccurred(error, `Error in _map`);
					return record;
				}
			});
		} catch (error) {
			this._errorOccurred(error, `Error during _applyMap`);
		}
	}
	_map(record) {
		return record; // override for custom map
	}
	_reduce(result) {

	}
	_onEnd(result) {
		this.success = true;
		this._onComplete();
	}
	_onComplete() {
		// meant to be overridden
	}
}

module.exports = global.Query = Query;


