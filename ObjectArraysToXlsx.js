const Excel = require('exceljs'); // https://www.npmjs.com/package/exceljs
const fs = require('fs'); // https://nodejs.org/api/fs.html
const path = require('path');
const xlsx = require('xlsx');

module.exports = async function(sheetName, objectArrays, filePath, fileName) {
let results = [];
	objectArrays.forEach(objArray => {
    	delete objArray[`attributes`];
    	results.push(onRecord(objArray));
	});
	results.forEach(res => {
	Object.keys(res).filter(key => {
				let value = res[key];
				if ((typeof value) == 'object' && value != null) 
				   delete res[key];
			});
	});
	
	if(fs.existsSync(filePath)){
		var wb = xlsx.readFile(filePath);
		var newWS = xlsx.utils.json_to_sheet(results);
		xlsx.utils.book_append_sheet(wb,newWS,sheetName);
		xlsx.writeFile(wb,filePath);
	}
	else{
		var newWB = xlsx.utils.book_new();
		var newWS = xlsx.utils.json_to_sheet(results);
		xlsx.utils.book_append_sheet(newWB,newWS,sheetName);
		xlsx.writeFile(newWB,filePath);

	}
}

function onRecord(record) {
		let relationshipFields = getRelationshipFields(record, null);
		relationshipFields.forEach(relationshipKey => {
			//console.log(`Relationship key: ${relationshipKey}`);
			let pathElements = relationshipKey.split(".");
			let value = pathElements.reduce((res,pathElement) => res[pathElement], record);
			record[relationshipKey] = value;
		});
		return record;
		// modify or use info from record.
	}

function getRelationshipFields(object, parentKey) {
		let relationshipKeys = Object.keys(object).filter(key => {
			return object[key] && (typeof object[key]) == `object` && key != `attributes`;
		});
		/*if (relationshipKeys.length > 0) {
			console.log("BOOM");
			console.log(relationshipKeys.map(key => { return {key, type: typeof object[key], value: object[key]}; }));
		}*/
		let relationshipFields = [];
		if (parentKey) {
			let leafKeys = Object.keys(object).filter(key => {
				return (typeof object[key]) != `object` && key != `attributes`;
			});
			leafKeys.forEach(leafKey => {
				//console.log(`Leaf key: ${leafKey}`);
				relationshipFields.push(parentKey + `.` + leafKey);
			});
		}
		relationshipKeys.forEach(key => {
			if (parentKey) {
				relationshipFields = relationshipFields.concat(getRelationshipFields(object[key], parentKey + "." + key));
			} else {
				relationshipFields = relationshipFields.concat(getRelationshipFields(object[key], key));
			}
		});
		return relationshipFields;
	}
