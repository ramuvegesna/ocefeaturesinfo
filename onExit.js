
let scriptStartTime;
const fs = require('fs'); // https://nodejs.org/api/fs.html

const directory = config.outputDirectory.suffixedIfNotAlready("/");


async function exitHandler(options, exitCode) {
    if (options.cleanup) console.log('-');
    if (exitCode || exitCode === 0) console.log(`Exit code: ${exitCode}`);
    if (options.scriptStartTime) {
    	let scriptEndTime = new Date();
    	let seconds = (scriptEndTime - scriptStartTime) / 1000;
    	console.log(`Script execution time: ${seconds}`);
    }
    
    if (options.exit) process.exit();
}

module.exports = function() {
	scriptStartTime = new Date();

	//do something when app is closing
	process.on('exit', exitHandler.bind(null,{cleanup:true, scriptStartTime}));
	
	//catches ctrl+c event
	process.on('SIGINT', exitHandler.bind(null, {exit:true, scriptStartTime}));
	
	// catches "kill pid" (for example: nodemon restart)
	process.on('SIGUSR1', exitHandler.bind(null, {exit:true, scriptStartTime}));
	process.on('SIGUSR2', exitHandler.bind(null, {exit:true, scriptStartTime}));
	
	//catches uncaught exceptions
	process.on('uncaughtException', exitHandler.bind(null, {exit:true, scriptStartTime}));
}