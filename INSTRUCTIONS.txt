1. Configure the application by following the instructions in HOW_TO_CONFIGURE.txt.

2. Double click OCEFeaturesInfo (on macOS) or OCEFeaturesInfo (on Windows) to run the application.

3. Application will delete previously generated files from the "Output" directory. If required, take backup before running the application.

4. Application will generate a excel file per org. in the "Output" directory. The name of the excel file will be same as name of the org. Each query output will be in a separate sheet in the excel file with the sheet name same as the query name. If there are errors while processing for an org. an error file will be generated with the error details for the org. The name of the error file will contain the name of the org.
