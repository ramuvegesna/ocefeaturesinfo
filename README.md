# OCE-Features-Info

### Notes
- This was written and tested on macOS

## Installation

1. Download repository
2. Navigate to root directory of the repository in terminal
3. Execute `npm install`

## Configuration
Example `config.json` with notes. (Real file cannot contain comments)
```javascript
{
    "outputDirectory": "./output/",
    "maximumRetryAttemptsOnFail": 2,
    "maximumActiveOrgs": 1000,
    "orgs": [
        { 
            "name": "Cool Name Org", 
            "username": "example.user@coolname.com", 
            "password": "ExamplePassword2019!", 
            "loginUrl": "https://login.salesforce.com" 
        }
    ],
    "queries": [
        { 
            "name": "Account",
            "query": "select Id,Name from Account limit 10"
        },
        { 
            "name": "Accounts between specific dates",
            "query": "Select Id,Name from Account where createddate > 2019-01-01T12:00:00.000Z"
        }    
    ] 
}
```

## Run script from code

1. Navigate to root directory of the repository in terminal
2. Execute `node OCEFeaturesInfo.js`

## Compile executable version

1. Navigate to root directory of the repository in terminal
2. Execute `node CompileOCEFeaturesInfo.js`
3. This will generate a directory called `compiled` and fill it with the files necessary for executable release.

## Files for executable release

- `OCEFeaturesInfo` (the executable file, not the `.js` file) for macOS or `OCEFeaturesInfo.exe` for Windows
- `config.json` (required at the time the script is executed to provide org credentials and other options)
- `OCEFeaturesInfo.command` for macOS or `OCEFeaturesInfo.bat` for Windows (double click to execute the script)
- `HOW_TO_CONFIGURE.txt` (to tell users how to configure the script)
- `INSTRUCTIONS.txt` (to tell users how to run the executable)

## Running executable

- Double click `OCEFeaturesInfo.command` for macOS or `OCEFeaturesInfo.bat` for Windows
