
const requires = require('./requires.js');
const Excel = require('exceljs'); // https://www.npmjs.com/package/exceljs
const path = require('path');
const fs = require('fs'); // https://nodejs.org/api/fs.html
const xlsx = require('xlsx');


let directory = config.outputDirectory.suffixedIfNotAlready("/");
createDirIfNotExists(directory);

let orgManager = new OrgManager(allOrgs);


async function main() {

	await orgManager.execute(async org => {
		executeData(org);
    });

}

cleanOutputDirectory(directory);
main();



async function executeData(org){
	config.queries.forEach(async qry => {
  	try {
	    	org.results = await org.execute(qry.query);
			if (org.results.records.length == 0){
				let records = {};
					records.Result = `No Records Found`;
				 org.results.records.push(records);
				}

				let syncFilename = `${org.name}.xlsx`;
				await objArraysToXlsx(qry.name, org.results.records, `${directory}${syncFilename}`,syncFilename);

  		}catch (error) {
    	writeErrorsFiles(org,error,qry);
  	}
  	});	
}


async function writeErrorsFiles(org,error,qry) {
	
	let errorFileContent = `--- ${qry.name}---\n`;
	errorFileContent += `${error.message} | ${error.error}\n\n\n`;

	let directory = config.outputDirectory.suffixedIfNotAlready("/");
	createDirIfNotExists(directory);
	let filename = `${directory}${org.name} Errors.txt`;
	if(!fs.existsSync(filename))
		fs.writeFileSync(filename, errorFileContent);
	else
		fs.appendFileSync(filename, errorFileContent);
}
