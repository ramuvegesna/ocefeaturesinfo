
const ObjectsToCsv = require('objects-to-csv');

module.exports = async function(objectArray, filePath) {

	// get rid of object values
	let newObjectArray = objectArray.map(object => {
		let newObject = Object.assign({}, object);
		Object.keys(newObject).forEach(key => {
			let value = newObject[key]
			if ((typeof value) == 'object' && value != null) {
				delete newObject[key];
			}
		});
		return newObject;
	});

	if (newObjectArray.length > 0) {
		// add headers
		let headerObject = Object.keys(newObjectArray[0]).reduce((res,key) => {
			res[key] = key;
			return res;
		}, {})
		newObjectArray.unshift(headerObject);
	}

	let csv = new ObjectsToCsv(newObjectArray);
	await csv.toDisk(filePath);
}