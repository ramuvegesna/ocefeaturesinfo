const fs = require('fs'); // https://nodejs.org/api/fs.html

global.config = JSON.parse(fs.readFileSync('./config.json')); // contains user input for credentials, dates, etc.

require('./extensions.js');
require('./Org.js');
require('./OrgManager.js');
require('./onExit.js')(); // on process exit handling stuff for debugging and timing.

require('./Query.js');

global.objArrayToCsv = require('./ObjectArrayToCsv.js');
global.objArraysToXlsx = require('./ObjectArraysToXlsx.js');


global.allOrgs = config.orgs
	.map(function(orgData) {
		let {name, username, password, loginUrl} = orgData;
		return new Org(name, username, password, loginUrl);
	});


function writeScriptErrorFile(error) {
	let errorFileContent = `--- SCRIPT ERRORS---\n`;
	errorFileContent += `${error}\n\n\n`;

	if (!fs.existsSync(dir)) 
   		fs.mkdirSync(dir);

	fs.writeFileSync(`${dir}Script Errors.txt`, errorFileContent);
}

module.exports = {};
