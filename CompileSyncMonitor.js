const { compile } = require('nexe')
require('./extensions.js');
const fs = require('fs');

let scriptName = `OCEFeaturesInfo`;
 
let input = `./${scriptName}.js`;

let compiledDirectory = `./compiled/`;

async function main() {
	createDirIfNotExists(compiledDirectory);

	await compile({
		input,
		output: `${compiledDirectory}${scriptName}`,
		target: 'macos'
	});

	await compile({
		input,
		output: `${compiledDirectory}${scriptName}.exe`,
		target: 'win32'
	});

	console.log(`Done compiling ${input}`);
	console.log(`Generating command and batch files.`);

	let writeOptions = { mode: 0o777 };

	// double-clickable file for macos that will run the script in the correct context.
	let commandFileName = `${compiledDirectory}${scriptName}.command`
	fs.writeFileSync(commandFileName, 
		`#!/bin/bash
		cd "$(dirname "$0")"
		./${scriptName}
		`, writeOptions);

	// double clickable file for windows that will run the script and keep the command window open at the end.
	let batchFileName = `${compiledDirectory}${scriptName}.bat`
	fs.writeFileSync(batchFileName,
		`.\\${scriptName}.exe
		pause
		`, writeOptions);

	console.log(`Copying relevant release files to ${compiledDirectory}`);

	fs.copyFileSync(`./defaultConfig.json`, `${compiledDirectory}config.json`);
	fs.copyFileSync(`./INSTRUCTIONS.txt`, `${compiledDirectory}INSTRUCTIONS.txt`);
	fs.copyFileSync(`./HOW_TO_CONFIGURE.txt`, `${compiledDirectory}HOW_TO_CONFIGURE.txt`);

	console.log(`Done!`);
}
main();

